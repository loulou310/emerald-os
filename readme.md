# Emerald OS

*<p style="text-align: center;">"Arch Linux, but with an installer plus cybersecurity tools"</p>*

<hr>

**Warning** : This project is in a **very early alpha**. It can occasionally break.

Emerald OS is a rolling release Linux distribution based on Arch Linux. It aims to be easily installable, with a modulable installation and ships the Archstrike repository, to have easy access to all you favourite cyber-security tools.

Right now, it is only a working Arch Linux iso that ships KDE Plasma, Firefox and Vim and is rebranded.

The software list of the ISO file is my personnal preference, but I'm open to suggestion.

There's no regular autobuilds for now, due to shared runner limitations.

## Building the "git" iso

The git iso is the newest version you can have. It builds dirrectly from the head of the repo

### Arch Linux

- Clone the repository  
- Install the package `extra/archiso`  
- In the directory, run with administrative privilleges `mkarchiso -w <work dir> -o <Output dir> ./`